﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaf : MonoBehaviour
{
    public static List<Leaf> WorldLeaves = new List<Leaf>();
    public static int LeavesInBaskets = 0;
    public static int TotalLeaves => WorldLeaves.Count;

    public static void ClearLeaves()
    {
        WorldLeaves = new List<Leaf>();
        Leaf.LeavesInBaskets = 0;
    }

    public static Leaf[] GetLeavesInBasket(LeafBasket basket)
    {
        List<Leaf> leafList = new List<Leaf>();
        foreach(Leaf leaf in WorldLeaves)
        {
            if(leaf.OwningBasket == basket)
            {
                leafList.Add(leaf);
            }
        }
        return leafList.ToArray();
    }

    private float _dropSpeed = 12.0f;
    private bool _moving = false;
    private Vector3 _destination;
    private LeafBasket _owningBasket = null;

    public bool InBasket => (_owningBasket != null ? true : false);
    public LeafBasket OwningBasket => _owningBasket;

    public void StopMoving()
    {
        _moving = false;
    }

    public void SetSprite(Sprite newSprite)
    {
        SpriteRenderer spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        spriteRenderer.sprite = newSprite;
    }

    public void DropIntoBasket(LeafBasket basket)
    {
        LeafBasket.Dimensions fillZone = basket.FillZoneDimensions;
        float randomX = Random.Range(fillZone.TopLeft.x, fillZone.TopLeft.x + fillZone.Width);
        float randomY = Random.Range(fillZone.TopLeft.y, fillZone.TopLeft.y - fillZone.Height);

        _moving = true;
        _destination = new Vector2(randomX, randomY);

        _owningBasket = basket;
        transform.parent = _owningBasket.LeafFillZone.transform;
        Leaf.LeavesInBaskets++;
    }

    public void FallOutOfBasket()
    {
        float maxFallOutAngle = Mathf.PI / 4;      // 45 degrees
        float maxFallOutDistance = 5.0f;
        float minFallOutDistance = 1.0f;

        float fallAngle = Random.Range(-maxFallOutAngle / 2.0f, maxFallOutAngle / 2.0f) - Mathf.PI / 180.0f * 45.0f;
        float fallDistance = Random.Range(minFallOutDistance, maxFallOutDistance);

        Vector2 relativeMove = new Vector2(_owningBasket.LeafFillZone.transform.up.x + Mathf.Sin(fallAngle), _owningBasket.LeafFillZone.transform.up.y) * fallDistance;
        Vector2 newPosition = (Vector2)transform.position + relativeMove;

        _moving = true;
        _destination = GameBorders.GetPointInBorders(newPosition);

        _owningBasket = null;
        transform.parent = null;
        Leaf.LeavesInBaskets--;

        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer sprite in sprites)
        {
            sprite.sortingOrder = 0;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        WorldLeaves.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        if(_moving)
        {
            transform.position += (_destination - transform.position).normalized * _dropSpeed * Time.deltaTime;

            if(((Vector2)_destination - (Vector2)transform.position).magnitude < 0.1f)
            {
                _moving = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (transform.parent != null)
        {
            if (collision.gameObject.name == "LeafFillZone")
            {
                SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
                foreach (SpriteRenderer sprite in sprites)
                {
                    sprite.sortingOrder = -1;
                }
            }
        }
    }
}
