﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafBasket : MonoBehaviour, IPandaInteraction
{
    public struct Dimensions
    {
        public Vector2 TopLeft;
        public float Width;
        public float Height;
    }

    [Tooltip("Panda standing point for tipping")]
    [SerializeField]
    private GameObject _pandaStandingPoint = null;

    [Tooltip("Area to fill in leaves")]
    [SerializeField]
    private GameObject _leafFillZone = null;

    private Animator _animator;
    private bool _isTipped = false;

    public bool IsTipped => _isTipped;
    public Vector2 PandaStandingPoint => _pandaStandingPoint.transform.position;
    public GameObject LeafFillZone => _leafFillZone;

    public Dimensions FillZoneDimensions
    {
        get
        {
            Dimensions myDimensions = new Dimensions();
            Transform fillZone = _leafFillZone.transform;
            myDimensions.TopLeft = (Vector2)fillZone.position + new Vector2(-fillZone.lossyScale.x / 2.0f, fillZone.lossyScale.y / 2.0f);
            myDimensions.Width = fillZone.lossyScale.x;
            myDimensions.Height = fillZone.lossyScale.y;
            return myDimensions;
        }
    }

    public PandaAction[] GetPandaActions()
    {
        List<PandaAction> actions = new List<PandaAction>();
        if ((_isTipped == false) && (GetComponentsInChildren<Leaf>().Length > 0))
        {
            WalkingPandaAction walkingAction = new WalkingPandaAction(_pandaStandingPoint.transform.position, true, false);
            KnockBasketPandaAction knockingAction = new KnockBasketPandaAction(this);
            actions.Add(walkingAction);
            actions.Add(knockingAction);
        }
        return actions.ToArray();
    }

    public void TipOver()
    {
        if (!_isTipped)
        {
            _animator.SetTrigger("Tip");
            _isTipped = true;
        }
    }

    public void Lift()
    {
        if (_isTipped)
        {
            _animator.SetTrigger("Lift");
            _isTipped = false;
        }
    }

    public void OnLeafFalloutAngle()
    {
        Debug.Log("Leaves fall out!");
        foreach(Leaf leaf in Leaf.GetLeavesInBasket(this))
        {
            leaf.FallOutOfBasket();
        }
    }

    void Start()
    {
        _animator = GetComponent<Animator>();
    }
}
