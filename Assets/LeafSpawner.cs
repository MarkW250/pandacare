﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafSpawner : MonoBehaviour
{

    [Tooltip("Prefab to use for leaves")]
    [SerializeField]
    private GameObject _leafPrefab;

    [Tooltip("Sprites that can be used for leaves")]
    [SerializeField]
    private Sprite[] _leafSprites;

    [Tooltip("Prefab to use for panda")]
    [SerializeField]
    private GameObject _pandaPrefab;

    private bool _leavesSpawned = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(_leavesSpawned == false)
        {
            _leavesSpawned = true;

            for (int i = 0; i < PlayerController.NumberOfLeavesToSpawn; i++)
            {
                // Get random location
                Vector3 spawnLoc = GameBorders.GetRandomPointInBorders();
                spawnLoc.z = -2.88f;

                // Pick random sprite
                int spriteIdx = Random.Range(0, _leafSprites.Length);

                GameObject newLeaf = GameObject.Instantiate(_leafPrefab, spawnLoc, new Quaternion());
                newLeaf.GetComponent<Leaf>().SetSprite(_leafSprites[spriteIdx]);
            }

            for (int i = 0; i < PlayerController.NumberOfPandasToSpawn; i++)
            {
                // Get random location
                Vector3 spawnLoc = GameBorders.GetRandomPointInBorders();
                GameObject.Instantiate(_pandaPrefab, spawnLoc, new Quaternion());
            }
        }
    }
}
