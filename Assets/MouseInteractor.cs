﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInteractor : MonoBehaviour
{
    public enum InteractorType
    {
        Leaf,
        Basket,
        Panda
    }

    /* Properties exposed to editor */
    [Tooltip("Interactor type")]
    [SerializeField]
    private InteractorType _interactorType = InteractorType.Leaf;

    public InteractorType Type => _interactorType;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
