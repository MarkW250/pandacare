﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class PandaAction
{
    public abstract string Name { get; }
    public abstract bool PerformAction(Panda panda);
}

public class WalkingPandaAction : PandaAction
{
    private Vector2 _walkingDestination;
    private bool _endDirection = true;      // true for left, false for right
    private bool _forceDir = false;         // force direction to face at end

    public WalkingPandaAction(Vector2 destination, bool forceDir = false, bool faceLeft = true)
    {
        _walkingDestination = destination;
        _endDirection = faceLeft;
        _forceDir = forceDir;
    }

    public override string Name => "Walking";


    public override bool PerformAction(Panda panda)
    {
        Debug.DrawLine(panda.transform.position, _walkingDestination, Color.black);
        Vector2 toDestination = (_walkingDestination - (Vector2)panda.transform.position).normalized * panda.WalkSpeed * Time.deltaTime;
        panda.transform.position = panda.transform.position + (Vector3)toDestination;

        if((_walkingDestination - (Vector2)panda.transform.position).x < 0.0f)
        {
            panda.GetComponent<PerspectiveControl>().NegateXScale(false);
        }
        else
        {
            panda.GetComponent<PerspectiveControl>().NegateXScale(true);
        }

        if ((_walkingDestination - (Vector2)panda.transform.position).magnitude < 0.2f)
        {

            if (_forceDir)
            {
                panda.GetComponent<PerspectiveControl>().NegateXScale(!_endDirection);
            }

            // Action finished
            panda.SetWalking(false);
            return true;
        }

        panda.SetWalking(true);
        return false;
    }
}


public class KnockBasketPandaAction : PandaAction
{
    LeafBasket _targetBasket;
    bool _knockingBasket = false;
    public KnockBasketPandaAction(LeafBasket targetBasket)
    {
        _targetBasket = targetBasket;
        _knockingBasket = false;
    }

    public override string Name => "Knocking basket";

    public override bool PerformAction(Panda panda)
    {
        if (((Vector2)panda.transform.position - _targetBasket.PandaStandingPoint).magnitude < 0.5f)
        {
            if(_knockingBasket == false)
            {
                // trigger panda to start playing knocking over basket
                panda.StartKnockingBasket(_targetBasket);
                _knockingBasket = true;
            }
            else
            {
                // Wait for Panda to finish knocking over basket animation
                if(panda.KnockingBasket == false)
                {
                    return true;
                }
            }
        }
        else
        {
            // Target basket is no longer in range for some reason. Rather then say action is complete
            return true;
        }

        return false;
    }
}

