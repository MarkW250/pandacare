﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



class DebugHelper
{
    public static void DrawCircle(Vector2 position, float radius, int resolution = 10)
    {
        Vector2 fromPosition = new Vector2(position.x, position.y + radius);

        float sliceAngle = 2 * Mathf.PI / resolution;

        for(int i = 0; i < resolution; i++)
        {
            float nextX = radius * Mathf.Sin(sliceAngle * (i + 1));
            float nextY = radius * Mathf.Cos(sliceAngle * (i + 1));

            nextX += position.x;
            nextY += position.y;
            Vector2 toPosition = new Vector2(nextX, nextY);

            Debug.DrawLine(fromPosition, toPosition);

            fromPosition = toPosition;
        }
    }
}

