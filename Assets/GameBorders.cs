﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameBorders : MonoBehaviour
{
    private static GameBorders _mainBorderControl = null;

    public static bool IsPointInGameBorders(Vector2 point)
    {
        float width = _mainBorderControl.transform.lossyScale.x;
        float height = _mainBorderControl.transform.lossyScale.y;
        float left = _mainBorderControl.transform.position.x - width / 2;
        float right = _mainBorderControl.transform.position.x + width / 2;
        float top = _mainBorderControl.transform.position.y + height / 2;
        float bottom = _mainBorderControl.transform.position.y - height / 2;

        if( (point.x < left) || 
            (point.x > right) || 
            (point.y < bottom) || 
            (point.y > top)
            )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static Vector2 GetRandomPointInBorders()
    {
        float width = _mainBorderControl.transform.lossyScale.x;
        float height = _mainBorderControl.transform.lossyScale.y;
        float left = _mainBorderControl.transform.position.x - width / 2;
        float right = _mainBorderControl.transform.position.x + width / 2;
        float top = _mainBorderControl.transform.position.y + height / 2;
        float bottom = _mainBorderControl.transform.position.y - height / 2;

        float randomX = Random.Range(left, right);
        float randomY = Random.Range(top, bottom);

        return new Vector2(randomX, randomY);
    }

    public static Vector2 GetPointInBorders(Vector2 point)
    {
        float width = _mainBorderControl.transform.lossyScale.x;
        float height = _mainBorderControl.transform.lossyScale.y;
        float left = _mainBorderControl.transform.position.x - width / 2;
        float right = _mainBorderControl.transform.position.x + width / 2;
        float top = _mainBorderControl.transform.position.y + height / 2;
        float bottom = _mainBorderControl.transform.position.y - height / 2;

        if(point.x < left)
        {
            point.x = left;
        }
        if (point.x > right)
        {
            point.x = right;
        }
        if (point.y < bottom)
        {
            point.y = bottom;
        }
        if (point.y > top)
        {
            point.y = top;
        }

        return point;
    }

    // Start is called before the first frame update
    void Start()
    {
        _mainBorderControl = this;
    }

    // Update is called once per frame
    [ExecuteInEditMode]
    void OnDrawGizmosSelected()
    {
        float width = transform.lossyScale.x / 2;
        float height = transform.lossyScale.y / 2;
        Vector2 topLeft =       (Vector2)transform.position + new Vector2(-width, height);
        Vector2 topRight =      (Vector2)transform.position + new Vector2(width, height);
        Vector2 bottomLeft =    (Vector2)transform.position + new Vector2(-width, -height);
        Vector2 bottomRight =   (Vector2)transform.position + new Vector2(width, -height);
        Gizmos.DrawLine(topLeft, topRight);
        Gizmos.DrawLine(bottomRight, topRight);
        Gizmos.DrawLine(bottomLeft, bottomRight);
        Gizmos.DrawLine(bottomLeft, topLeft);
    }


}
