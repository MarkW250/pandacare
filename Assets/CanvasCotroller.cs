﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasCotroller : MonoBehaviour
{
    [Tooltip("Leaves Collected Text Object")]
    [SerializeField]
    Text _leavesCollectedText = null;

    [Tooltip("Level Text Object")]
    [SerializeField]
    Text _levelText = null;

    [SerializeField]
    [Tooltip("Dark Overlay Object")]
    GameObject _darkOverlay = null;

    [SerializeField]
    [Tooltip("Pause screen object")]
    GameObject _pauseScreen = null;

    [SerializeField]
    [Tooltip("Pause screen object")]
    GameObject _titleScreen = null;

    [SerializeField]
    [Tooltip("Game win screen object")]
    GameObject _gameWinScreen = null;

    [SerializeField]
    [Tooltip("Game Lose screen object")]
    GameObject _gameLoseScreen = null;

    public void SetPauseScreen(bool active)
    {
        _darkOverlay.SetActive(active);
        _pauseScreen.SetActive(active);
    }

    public void SetTitleScreen(bool active)
    {
        _darkOverlay.SetActive(active);
        _titleScreen.SetActive(active);
    }

    public void SetWinScreen(bool active)
    {
        _darkOverlay.SetActive(active);
        _gameWinScreen.SetActive(active);
    }

    public void SetLoseScreen(bool active)
    {
        _darkOverlay.SetActive(active);
        _gameLoseScreen.SetActive(active);
    }
    private string _originaleavesStr;

    // Start is called before the first frame update
    void Start()
    {
        _originaleavesStr = _leavesCollectedText.text;
    }

    // Update is called once per frame
    void Update()
    {
        updateLeavesCollectedText();
        updateLevelText();
    }

    private void updateLeavesCollectedText()
    {
        string leavesCollectedStr = string.Format("{0}{1} / {2}", _originaleavesStr, Leaf.LeavesInBaskets, Leaf.TotalLeaves);
        _leavesCollectedText.text = leavesCollectedStr;
    }

    private void updateLevelText()
    {
        if (_levelText != null)
        {
            string levelStr = string.Format("Level {0}", PlayerController.Instance.Level);
            _levelText.text = levelStr;
        }
    }

}
