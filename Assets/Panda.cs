﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panda : MonoBehaviour
{

    /* Properties exposed to editor */
    [Tooltip("Panda detection/walk radius")]
    [SerializeField]
    private float _detectionRadius = 5.0f;

    [Tooltip("Panda walk speed")]
    [SerializeField]
    private float _walkSpeed = 2.0f;

    private Animator _animator = null;

    /* Private settings */
    float _decisionRate = 0.1f;             /* Number of milliseconds to run decision making */
    float _startWalkingProbability = 0.05f; /* Probability tht the panda will decide to walk */
    float _interactProbability = 0.5f;      /* Probability tht the panda will decide to interact with something */

    /* Private state variables */
    float _decisionTimer = 0.0f;
    Queue<PandaAction> _actionQueue = new Queue<PandaAction>(4);
    PandaAction _currentAction = null;
    LeafBasket _knockingBasket = null;
    bool _pickedUp = false;

    /* Private state variables for simulating animation time */
    float _animationTime = 1.0f;
    float _tipPointTime = 0.6f;
    float _animationTimer = 0.0f;
    bool _tipped = false;

    public float WalkSpeed => _walkSpeed;
    public bool KnockingBasket => (_knockingBasket != null ? true : false);


    public void SetWalking(bool walking)
    {
        _animator.SetBool("walking", walking);
    }

    public void StartKnockingBasket(LeafBasket basket)
    {
        if (basket.IsTipped == false)
        {
            _knockingBasket = basket;
            _animator.SetTrigger("baskettip");
        }
    }

    public void OnFinishedKnockingBasket()
    {
        _knockingBasket = null;
    }

    public void OnBasketStartTipping()
    {
        _knockingBasket.TipOver();
    }

    public void OnPickUp()
    {
        _pickedUp = true;
        _animator.SetBool("pickedup", true);
    }

    public void OnDrop()
    {
        // make sure we are being dropped within game borders
        this.transform.position = GameBorders.GetPointInBorders(this.transform.position);

        _animator.SetBool("pickedup", false);
    }

    public void OnFinishedDrop()
    {
        _pickedUp = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        _knockingBasket = null;
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerController.IsGamePaused())
        {
            return;
        }
        // do nothing if picked up
        if(_pickedUp == true)
        {
            return;
        }

        // temporary timer for simulating animation time
       // SimulateAnimationTime();

        // Create detection circle for debugging
        DebugHelper.DrawCircle(this.transform.position, _detectionRadius, 50);

        // Get interactive objecs in detection radius
        int selectionLayerMask = LayerMask.GetMask("PandaInteractive");
        Collider2D[] hits = Physics2D.OverlapCircleAll(this.transform.position, _detectionRadius, selectionLayerMask);

        foreach (Collider2D hit in hits)
        {
            Vector2 objectLocation = hit.transform.position;
            Debug.DrawLine(this.transform.position, objectLocation, Color.red);
        }

        if((_currentAction == null) && (_actionQueue.Count > 0))
        {
            _currentAction = _actionQueue.Dequeue();
            Debug.Log("Starting Action: " + _currentAction.Name);
        }

        if(_currentAction != null)
        {
            if(_currentAction.PerformAction(this))
            {
                Debug.Log("Finished Action: " + _currentAction.Name);
                _currentAction = null;
            }
        }
        else
        {
            // Check for random decisions
            _decisionTimer += Time.deltaTime;
            if (_decisionTimer > _decisionRate)
            {
                _decisionTimer = 0.0f;
                // Check if panda wants to walk somewhere
                float randomNumber = Random.value;

                if (randomNumber < _startWalkingProbability)
                {
                    WalkRandomly();
                }
                else if(randomNumber < (_startWalkingProbability + _interactProbability))
                {
                    InteractWithObject(hits);
                }
            }
        }
    }

    private void InteractWithObject(Collider2D[] hits)
    {
        // interact with item in range (if any)
        int i = 0;
        IPandaInteraction pandaInteraction = null;
        while ((pandaInteraction == null) && (i < hits.Length))
        {
            pandaInteraction = hits[i++].transform.GetComponent<IPandaInteraction>();
        }
        if (pandaInteraction != null)
        {
            PandaAction[] actions = pandaInteraction.GetPandaActions();
            foreach (PandaAction action in actions)
            {
                _actionQueue.Enqueue(action);
            }
        }
    }

    private void WalkRandomly()
    {
        // Randomly walk to position in detection radius
        Vector2 walkingDestination = (Random.insideUnitCircle * _detectionRadius) + (Vector2)transform.position;
        walkingDestination = GameBorders.GetPointInBorders(walkingDestination);
        WalkingPandaAction newAction = new WalkingPandaAction(walkingDestination);
        _actionQueue.Enqueue(newAction);
    }

    private void SimulateAnimationTime()
    {
        if(_knockingBasket != null)
        {
            _animationTimer += Time.deltaTime;
            if(_animationTimer > _tipPointTime)
            {
                if (_tipped == false)
                {
                    // trigger tipping of basket
                    OnBasketStartTipping();
                    _tipped = true;
                }
            }
            if(_animationTimer > _animationTime)
            {
                OnFinishedKnockingBasket();
            }
        }
        else
        {
            _animationTimer = 0.0f;
            _tipped = false;
        }
    }
}
