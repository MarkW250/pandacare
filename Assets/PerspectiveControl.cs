﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PerspectiveControl : MonoBehaviour
{
    [Tooltip("Original Scale")]
    [SerializeField]
    private Vector2 _originalScale = new Vector2(1,1);

    [Tooltip("Take care of scale for distancing")]
    [SerializeField]
    private bool _scaleObject = true;


    /* Private settings */
    float _scaleFactor = 0.03f;

    bool _negateXScale = false;

    public void NegateXScale(bool negateXScale)
    {
        _negateXScale = negateXScale;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    
    void Update()
    {
        float zOffset = 0;
        if(GetComponent<Leaf>() != null)
        {
            // if we are a leaf, then we try to always be infront of stuff, so add a little something to z to come on top most of the time
            if (PlayerController.Instance.IsObjectBeingPickedUp(gameObject))
            {
                zOffset = -5.0f;
            }
            else
            {
                zOffset = -0.05f;
            }
        }

        if (transform.parent != null)
        {
            return;
        }
        else
        {
            Vector3 newPosition = transform.position;
            newPosition.z = newPosition.y + zOffset;
            transform.position = newPosition;
        }

        if (_scaleObject)
        {
            float newScaleFactor = (1 - _scaleFactor * (transform.position.z));
            Vector2 newScale = _originalScale * newScaleFactor;
            if(_negateXScale)
            {
                newScale.x = newScale.x * -1.0f;
            }
            transform.localScale = newScale;
        }
    }
}
