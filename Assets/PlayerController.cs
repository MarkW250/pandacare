﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [Tooltip("Interaction Radius")]
    [SerializeField]
    private float _interactionRadius = 0.5f;

    /* Private settings */
    int _maxCarryLeaves = 20;
    int _level = 0;

    /* Private state variables */
    bool _holdingObject = false;
    GameObject[] _objectsInHand;
    Vector2[] _leafOffsets;
    int _leafCount = 0;
    bool _gameEnded = false;
    float _gameResetTimer = 0.0f;
    bool _paused = false;
    CanvasCotroller _uiController = null;
    bool _title = false;

    public static PlayerController Instance;
    public static bool IsGamePaused()
    {
        return Instance.IsPaused();
    }

    public bool IsPaused()
    {
        return _paused || _gameEnded;
    }

    public bool IsObjectBeingPickedUp(GameObject obj)
    {
        if (!_holdingObject)
        {
            return false;
        }

        for (int i = 0; i < _leafCount; i++)
        {
            if(_objectsInHand[i] == obj)
            {
                return true;
            }
        }

        return false;
    }

    public int LeavesToSpawn => (_level - 1) * 5 + 20;
    public static int NumberOfLeavesToSpawn => Instance.LeavesToSpawn;

    public int PandasToSpawn => (_level);
    public static int NumberOfPandasToSpawn => Instance.PandasToSpawn;

    public int Level => _level;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerController.Instance == null)
        {
            PlayerController.Instance = this;
            GameObject.DontDestroyOnLoad(this);
        }
        else if(PlayerController.Instance != this)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }

        SceneManager.sceneLoaded += this.SceneLoaded;

        StartNextLevel();
    }

    private void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        StartNextLevel();
    }

    private void StartNextLevel()
    {
        _gameEnded = false;
        _level++;
        Leaf.ClearLeaves();

        _objectsInHand = new GameObject[_maxCarryLeaves];
        _leafOffsets = new Vector2[_maxCarryLeaves];

        _uiController = GetComponentInChildren<CanvasCotroller>();

        _uiController.SetWinScreen(false);
        _uiController.SetLoseScreen(false);

        if (_level == 1)
        {
            showTitle();
        }
        else
        {
            _uiController.SetTitleScreen(false);
        }
    }

    private void showTitle()
    {
        _title = true;
        _paused = true;
        _uiController.SetTitleScreen(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (HandlePause())
        {
            return;
        }

        Vector2 mousePos2D = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Create detection circle for debugging
        DebugHelper.DrawCircle(mousePos2D, _interactionRadius, 20);

        if (_holdingObject)
        {
            for(int i = 0; i < _leafCount; i++)
            {
                // position leaves randomly around mouse pointer
                Vector2 leafPosition = mousePos2D + _leafOffsets[i];
                _objectsInHand[i].transform.position = new Vector3 (leafPosition.x, leafPosition.y, _objectsInHand[i].transform.position.z);
            }

            if (Input.GetMouseButtonUp(0))
            {
                _holdingObject = false;

                // were we carrying leaves?
                if (_objectsInHand[0].GetComponent<Leaf>() != null)
                {
                    // check if reelase was on leaf drop zone
                    int selectionLayerMask = LayerMask.GetMask("LeafDropZone");
                    RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero, 1000.0f, selectionLayerMask);

                    if (hit.collider != null)
                    {
                        // leaves must fall into basket
                        dropLeavesInBasket(hit.transform.parent.parent.parent.gameObject);

                        if(Leaf.LeavesInBaskets >= Leaf.TotalLeaves)
                        {
                            gameWin();
                        }
                    }
                }
                else if(_objectsInHand[0].GetComponent<Panda>() != null)
                {
                    // we were carrying a panda -> just let go of panda
                    _objectsInHand[0].GetComponent<Panda>().OnDrop();
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                Collider2D[] hits = Physics2D.OverlapCircleAll(mousePos2D, _interactionRadius);

                bool pickingLeaves = false;
                int leafIndex = 0;
                bool exitLoop = false;

                foreach (Collider2D hit in hits)
                {
                    MouseInteractor interactor = hit.transform.GetComponent<MouseInteractor>();
                    if (interactor != null)
                    {
                        switch (interactor.Type)
                        {
                            case MouseInteractor.InteractorType.Leaf:
                                if (!hit.transform.GetComponent<Leaf>().InBasket)
                                {
                                    hit.transform.GetComponent<Leaf>().StopMoving();
                                    pickingLeaves = true;
                                    _holdingObject = true;
                                    _objectsInHand[leafIndex] = interactor.gameObject;
                                    _leafOffsets[leafIndex] = Random.insideUnitSphere * (0.05f * (leafIndex < 8 ? leafIndex : 8));
                                    leafIndex++;
                                }
                                break;

                            case MouseInteractor.InteractorType.Basket:
                                if (pickingLeaves == false)
                                {
                                    if (hit.transform.parent.parent.GetComponent<LeafBasket>().IsTipped == true)
                                    {
                                        hit.transform.parent.parent.GetComponent<LeafBasket>().Lift();
                                        exitLoop = true;
                                    }
                                }
                                break;

                            case MouseInteractor.InteractorType.Panda:
                                if (pickingLeaves == false)
                                {
                                    _holdingObject = true;
                                    _objectsInHand[0] = interactor.transform.parent.gameObject;
                                    _leafOffsets[0] = (Vector2)interactor.transform.parent.position - mousePos2D;
                                    hit.transform.parent.GetComponent<Panda>().OnPickUp();
                                    exitLoop = true;
                                    leafIndex = 1;
                                }
                                break;

                            default:
                                break;
                        }

                        if ((exitLoop == true) || (leafIndex >= _maxCarryLeaves))
                        {
                            // can break out of loop if we are not picking up leaves or reached maximum leaves to pick up
                            break;
                        }
                    }

                    
                }
                _leafCount = leafIndex;
            }
        }
    }

    private void dropLeavesInBasket(GameObject targetBasket)
    {
        // Get leaf basket component
        LeafBasket basket = targetBasket.GetComponent<LeafBasket>();

        if (!basket.IsTipped)
        {
            // For each leaf, drop to random location within fill zone
            for (int i = 0; i < _leafCount; i++)
            {
                Leaf leaf = _objectsInHand[i].GetComponent<Leaf>();
                leaf.DropIntoBasket(basket);
            }
        }

    }


    bool HandlePause()
    {
        bool escPressed = Input.GetKeyDown(KeyCode.Escape);
        bool primaryPressed = Input.GetMouseButtonDown(0);
        bool qPressed = Input.GetKeyDown(KeyCode.Q);

        if (_gameResetTimer > 0.0f)
        {
            _gameResetTimer -= Time.deltaTime;
        }

        if (_gameEnded)
        {
            if (escPressed || qPressed)
            {
                Application.Quit();
            }
            else if (primaryPressed)
            {
                if (_gameResetTimer <= 0.0f)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }
        else
        {
            if (_paused)
            {
                if (escPressed || primaryPressed)
                {
                    if (_title)
                    {
                        _uiController.SetTitleScreen(false);
                        unpause();
                        _title = false;
                    }
                    else
                    {
                        unpause();
                    }
                }
                else if (qPressed)
                {
                    Application.Quit();
                }
            }
            else
            {
                if (escPressed)
                {
                    pause();
                }
            }
        }
        return (_paused || _gameEnded);
    }

    private void gameWin()
    {
        _gameEnded = true;
        _uiController.SetWinScreen(true);
        _gameResetTimer = 1.0f;
    }

    private void gameLose()
    {
        _gameEnded = true;
        _gameResetTimer = 1.0f;
    }

    private void pause()
    {
        _uiController.SetPauseScreen(true);
        _paused = true;
        Time.timeScale = 0;
    }

    private void unpause()
    {
        _uiController.SetPauseScreen(false);
        _paused = false;
        Time.timeScale = 1;
    }
}
